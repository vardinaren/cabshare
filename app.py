from flask import Flask, request, render_template, session, url_for, redirect, flash
from LoginForm import login_wtf_class
from helper_functions import isLoginValid
from functools import wraps

app = Flask(__name__)
app.secret_key = 'ca\xc2\n\x98!\xad\xcek\xd7\xa8\xbc\x0f\xa3\x8cs\x8c\xf6\x9d\xfb\x9d\xd2\\\x8b'

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'email' in session:
            return f(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login'))
    return wrap

@app.route('/')
@login_required
def home():
    return render_template('parent.html')

@app.route('/PickUp')
@login_required
def pickup():
    return url_for('pickup')

@app.route('/RequestPickUp')
@login_required
def requestpickup():
    return url_for('requestpickup')

@app.route('/login',methods=['GET','POST'])
def login():
    login_form = login_wtf_class(request.form)
    if request.method.lower() == "POST".lower() and login_form.validate():
        login_email = login_form.email.data
        login_password = login_form.password.data
        if isLoginValid(login_email,login_password):
            session['email'] = str(login_email)
            user_has_a_car = login_form.own_a_vehicle.data
            if user_has_a_car.lower() == 'NO'.lowerequestpickupr():
                return redirect(url_for(''))
            else:
                return redirect(url_for('pickup'))
    else:
        return render_template("LoginForm.html",login_form=login_form)

@app.route('/logout')
@login_required
def logout():
    session.pop('email', None)
    return redirect(url_for('home'))

if __name__ == '__main__':
    app.run(debug=True)
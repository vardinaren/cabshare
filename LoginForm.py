from wtforms import Form, BooleanField, StringField, PasswordField, validators, RadioField


class login_wtf_class(Form):
    email = StringField('Email Address', [validators.Email("Please enter valid email id:")])
    password = PasswordField('Password', [validators.Length(min=8, max=35),
                                          validators.DataRequired()
                                          ])
    own_a_vehicle = RadioField('Do you have a vehicle?', [validators.DataRequired("Please select one option")],
                               choices=[('Yes', 'Yes'), (' No', 'No')])
